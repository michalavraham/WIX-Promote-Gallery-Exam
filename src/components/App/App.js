import React from 'react';
import './App.scss';
import Gallery from '../Gallery';

class App extends React.Component {
  static propTypes = {
  };

  constructor() {
    super();
    this.state = {
      tag: 'art'
    };
    this.timeout = null;
  }

  searchHandler(event) {
    let searchText = event.target.value;
    if (this.timeout) clearTimeout(this.timeout); 
    this.timeout = setTimeout(() => { 
      this.setState({ tag: searchText }); 
    }, 500);
  }

  render() {
    
    return (
      <div className="app-root">
        <div className="app-header">
          <h2>Flickr Gallery</h2>
          <input className="app-input" onChange={event => this.searchHandler(event)} />
        </div>
        <Gallery tag={this.state.tag} />
      </div>
    );
  }
}

export default App;
