import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Image from '../Image';
import './Gallery.scss';
import FontAwesome from 'react-fontawesome';
class Gallery extends React.Component {
  static propTypes = {
    tag: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      images: [],
      galleryWidth: this.getGalleryWidth(),
      imageUrl: '',
      modalClass: 'myModal modalHide',
      modalOpen: false,
      pages: 1
    };
  }

  getGalleryWidth() {
    try {
      return document.body.clientWidth;
    } catch (e) {
      return 1000;
    }
  }

  getImages(tag, pages) {
    if (tag == '') tag = 'art';
    const getImagesUrl = `services/rest/?method=flickr.photos.search&api_key=522c1f9009ca3609bcbaf08545f067ad&tags=${tag}&page=${pages}&tag_mode=any&per_page=32&format=json&safe_search=1&nojsoncallback=1`;
    const baseUrl = 'https://api.flickr.com/';
    axios({
      url: getImagesUrl,
      baseURL: baseUrl,
      method: 'GET'
    })
      .then(res => res.data)
      .then(res => {
        if (
          res &&
          res.photos &&
          res.photos.photo &&
          res.photos.photo.length > 0
        ) {
          if (pages == 1) {
            this.setState({ images: res.photos.photo,pages:pages + 1});
          } else {
            this.setState({
              images: [...this.state.images, ...res.photos.photo],
              pages: this.state.pages + 1
            });
          }
        }
      });
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
    this.setState({
      galleryWidth: document.body.clientWidth
    });
  }

  componentWillReceiveProps(props) {
    this.getImages(props.tag,1);
  }

  cloneImage = (image, index) => {
    const imagesArray = this.state.images;
    imagesArray.splice(index, 0, image);
    this.setState({ images: imagesArray });
  }

  modalControll = (image) => {
    if (!this.state.modalOpen) {
      this.setState({
        modalOpen: true,
        modalClass: 'myModal modalShow',
        imageUrl: image
      });
    } else {
      this.setState({
        modalOpen: false,
        modalClass: 'myModal modalHide'
      });
    }
  }

  handleScroll = () => {
    if ((Math.ceil(document.documentElement.scrollTop) + document.documentElement.clientHeight) === document.documentElement.scrollHeight) {
      this.getImages(this.props.tag, this.state.pages);
    }
  };

  render() {
    return (
      <div className="gallery-root flexBox">
        {this.state.images.map((dto, index) => {
          return <Image
            key={'image-' + index}
            dto={dto}
            modalControll={this.modalControll}
            galleryWidth={this.state.galleryWidth}
            cloneImage={this.cloneImage}
            index={index}
          />
        })}

        <div className={this.state.modalClass}>
          <img src={this.state.imageUrl} />
          <div className="closeModal">
            <FontAwesome
              className="image-icon"
              name="window-close"
              title="close"
              onClick={this.modalControll}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Gallery;
