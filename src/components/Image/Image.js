import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import './Image.scss';

class Image extends React.Component {
  static propTypes = {
    dto: PropTypes.object,
    galleryWidth: PropTypes.number,
    cloneImage: PropTypes.func,
    index:PropTypes.number,
    modalControll: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.calcImageSize = this.calcImageSize.bind(this);
    this.state = {
      size: 200,
      filtersArray: [`saturate(8)`, `brightness(200%)`, `hue-rotate(90deg)`, `invert(100%)`, `grayscale(100%)`],
      randomFilter: ''
    };
  }

  calcImageSize() {
    const { galleryWidth } = this.props;
    const targetSize = 200;
    const imagesPerRow = Math.round(galleryWidth / targetSize);
    const size = (galleryWidth / imagesPerRow);
    this.setState({
      size
    });
  }

  componentDidMount() {
    this.calcImageSize();
  }

  urlFromDto(dto) {
    return `https://farm${dto.farm}.staticflickr.com/${dto.server}/${dto.id}_${dto.secret}.jpg`;
  }

  filterHanlder = () => {
    this.setState({ randomFilter: this.state.filtersArray[Math.floor(Math.random() * 5)] });
  }

  sendToCloneImage = () => {
    this.props.cloneImage(this.props.dto, this.props.index);
  }

  sendToModalControll = ()=>{
    this.props.modalControll(this.urlFromDto(this.props.dto));
  }
  render() {
    return (
      <div
        className='image-root'
        style={{
          backgroundImage: `url(${this.urlFromDto(this.props.dto)})`,
          width: this.state.size + 'px',
          height: this.state.size + 'px',
          filter: this.state.randomFilter
        }}
      >
        <div>
          <FontAwesome className="image-icon" name="clone" title="clone" onClick={this.sendToCloneImage} />
          <FontAwesome className="image-icon" name="filter" title="filter" onClick={this.filterHanlder} />
          <FontAwesome className="image-icon" name="expand" title="expand" onClick={this.sendToModalControll}/>
        </div>
      </div>
    );
  }
}

export default Image;
